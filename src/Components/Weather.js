import React from 'react';

const Weather = (props) => {
    return (
        <div className="container">
            <div className="cards">
                
                <h1>{props.city}</h1>
                
                <i className={ `icons wi ${props.weatherIcon} display-1 `} />
                {props.temp_celsius? <h1 className="temprature">{props.temp_celsius}&deg;C</h1> : null }
                {minMaxTemp(props.temp_min ,props.temp_max)}
                <h4>{props.description}</h4>
            </div>
        </div>
    );
}
const minMaxTemp=(min,max)=>{
    if (max && min) {
    return(
        <h3>
            <span className="mintemp">Min: {min}&deg;C  </span>
            
            <span className="maxtemp">Max: {max}&deg;C</span>
        </h3>
    )
    }
}
export default Weather;
