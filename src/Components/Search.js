import React from 'react';

const Search = (props) => {
    return (
        <div className='search'>
            <h1 className='weatherHeading'>Find Weather of your city</h1>
            <div className='searchError'>{props.error? error() :null}</div>
            <div className='container-search'>
                <form onSubmit={props.loadweather}>
                    <div className='form-row'>
                            <input type="text" placeholder='City' name="city"></input>
                    </div>
                    <div className='form-row'>
                            <input type="text" placeholder='Country' name="country"></input>
                    </div>
                    <div className='form-row'>
                        <button>Search</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

const error=()=>{
    return(
        <div>Please Enter the City and Country</div>
    )
}

export default Search;
