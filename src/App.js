
import React, { Component } from 'react';
import './App.css';
import Loader from './Components/loader';
import Search from './Components/Search';
import Weather from './Components/Weather';
import './weather-icons-master/css/weather-icons.css'


const API_key='be30a7c81ad73da495f8acb61c504354'
class App extends React.Component {
  constructor(){
    super()
    this.state={
      city:undefined,
      country:undefined,
      icon:undefined,
      main:undefined,
      celsius:undefined,
      temp_max:undefined,
      temp_min:undefined,
      description:"",
      error:false,
      loading:false,
      // isClicked: false
    }
    // this.getWeather()
    this.weatherIcon = {
      Thunderstorm: "wi-thunderstorm",
      Drizzle: "wi-sleet",
      Rain: "wi-storm-showers",
      Snow: "wi-snow",
      Atmosphere: "wi-fog",
      Clear: "wi-day-sunny",
      Clouds: "wi-day-fog"
    };
  }
  get_WeatherIcon(icons, rangeId) {
    switch (true) {
      case rangeId >= 200 && rangeId < 232:
        this.setState({ icon: icons.Thunderstorm });
        break;
      case rangeId >= 300 && rangeId <= 321:
        this.setState({ icon: icons.Drizzle });
        break;
      case rangeId >= 500 && rangeId <= 521:
        this.setState({ icon: icons.Rain });
        break;
      case rangeId >= 600 && rangeId <= 622:
        this.setState({ icon: icons.Snow });
        break;
      case rangeId >= 701 && rangeId <= 781:
        this.setState({ icon: icons.Atmosphere });
        break;
      case rangeId === 800:
        this.setState({ icon: icons.Clear });
        break;
      case rangeId >= 801 && rangeId <= 804:
        this.setState({ icon: icons.Clouds });
        break;
      default:
        this.setState({ icon: icons.Clouds });
    }}
  
  calCelsius(temp){
    let cell=Math.floor(temp-273.15)
    return cell
  }



  getWeather=async(event)=>{
    event.preventDefault()
    const country=event.target.elements.country.value
    const city=event.target.elements.city.value
    
    if(city&&country){
      this.setState({
        loading:true
      })
    }
    if(city&&country){
      const api_call=await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_key}`)
      const response=await api_call.json();

      this.setState({
        loading:false
      })

      
      if(response.cod!=="404"){
        this.setState({
          city:`${response.name},${response.sys.country}`,
          celsius: this.calCelsius(response.main.temp),
          temp_max: this.calCelsius(response.main.temp_max),
          temp_min: this.calCelsius(response.main.temp_min),
          description:response.weather[0].description,
          error:false,
        })
        this.get_WeatherIcon(this.weatherIcon, response.weather[0].id);
      
      }
      else{
        console.log("error")
        this.setState({
          city:"City not found, we are working to add more to the list",
          country:undefined,
          icon:undefined,
          main:undefined,
          celsius:undefined,
          temp_max:undefined,
          temp_min:undefined,
          description:"",
          error:false
        });
      }
    }
    else{
      this.setState({error:true})
    }
   };

  

  
  render() {
    const {loading} = this.state
    return (
      <div className="App">
        <Search loadweather={this.getWeather} error={this.state.error}/>
        {loading === true ? <Loader/> : 
        <Weather city={this.state.city} country={this.state.country} 
        temp_celsius={this.state.celsius}
        temp_max={this.state.temp_max}
        temp_min={this.state.temp_min}
        description={this.state.description.toUpperCase()}
        weatherIcon={this.state.icon}
        />}
      </div>
    );
  }
}

export default App;

